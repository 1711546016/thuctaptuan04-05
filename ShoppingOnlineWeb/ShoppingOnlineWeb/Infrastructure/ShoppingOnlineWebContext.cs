﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ShoppingOnlineWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingOnlineWeb.Infrastructure
{
    public class ShoppingOnlineWebContext : IdentityDbContext<AppUser>
    {
        public ShoppingOnlineWebContext(DbContextOptions<ShoppingOnlineWebContext> options)
            : base(options)
        {
        }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
